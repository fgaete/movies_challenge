# Challenge de CLM Consultures
## _API Rest de películas_

## Puntos realizados del challenge

- Se montó todo a nivel fullstack
- En la página principal se listan las películas que son agregadas a la BD Mongo
- En la pestaña "Agregar Películas" se pueden insertar películas que son obtenidas desde la API enviada
- En la página principal existe un buscador de películas que obtiene películas por alcance de nombre
- Faltó la parte encargada de actualizar el plot

## Instalación

Se debe descargar el repositorio en su computadora, para eso es necesario tener instalado git (https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
Para funcionar en ambiente local es necesario [Node.js](https://nodejs.org/) v10+.
Además se debe instalar el motor de MongoDB (https://docs.mongodb.com/manual/installation/) para poder utilizar la aplicación. Esto conlleva modificar el archivo app.js en la línea 43 en donde cambiaremos esto:
```sh
const connection = mongoose.connect('mongodb://db/movies_challenge',{useNewUrlParser: true, useUnifiedTopology: true})
```
Por esto:
```sh
const connection = mongoose.connect('mongodb://localhost:27017/movies_challenge',{useNewUrlParser: true, useUnifiedTopology: true})
```
Para así poder continuar con la base de datos local de MongoDB.

Posterior a eso se deben instalar las dependencias.

```sh
git clone https://gitlab.com/fgaete/movies_challenge.git
cd movies_challenge
npm install
npm start
```
## Docker

La aplicación también se puede utilizar con Docker puesto que en el proyecto viene el respectivo Dockerfile y docker-compose.yml
Para esto previamente se requiere tener instalado Docker (https://docs.docker.com/get-docker/) en sus equipos, así también Docker Compose (https://docs.docker.com/compose/install/) para ejecutar el archivo YAML

```sh
cd movies_challenge
docker-compose up -d
```

Esto va a crear dos contenedores, uno correspondiente a la base de datos en MongoDB y otra correspondiente a la aplicación

Pueden verificar que todo se encuentra funcionando con la siguiente URL en su navegador favorito 

```sh
127.0.0.1:3200
```

## Utilización

Esta aplicación cuenta con la siguiente navegación

```sh
127.0.0.1:3200/
127.0.0.1:3200/add
```

En donde la primera corresponde a la página principal que lista a las películas que se encuentran en nuestra BD y además posee un botón para buscar películas que ya fueron previamente guardadas.
En la segunda tenemos la página para agregar películas en donde uno ingresa el nombre de una película y esta es buscada en la API y rescata los datos para posteriormente insertarlos en la BD.