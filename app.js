//se importan dependencias y librerías
const Koa = require('koa');
const Router = require('koa-router');
const path = require('path');
const render = require('koa-ejs');
const mongoose = require('mongoose');
const Movies = require('./models/movie');
const bodyParser = require('koa-bodyparser');
const request = require('request');
//se inicializan constantes y variables
const app = new Koa();
const router = new Router();
var port = 3000;
const apikey = '54f94c94';
const url = 'http://www.omdbapi.com/?apikey=';
//var movie = new Movies();

//Middleware de BodyParser
app.use(bodyParser());

render(app,{
    root: path.join(__dirname,'views'),
    layout: 'layout',
    viewExt: 'html',
    cache: false,
    debug: false
})

//Crear y cargar lista con API externa
//const movies = ['Star Wars','The Avengers','El Manso Asao'];

//Rutas
router.get('/',index);
router.post('/search',search);
router.get('/add',showAdd);
router.post('/add',add);
router.post('/update',update);

//Lista de películas insertadas
const listaID = [];

//Se crea conexión con MongoDB
const connection = mongoose.connect('mongodb://db/movies_challenge',{useNewUrlParser: true, useUnifiedTopology: true})
        .then(()=>{
            console.log("Conectado a BD correctamente!");
            
        })
        .catch(err=> console.log(err));

//Middleware de rutas

//Index Page
async function index(ctx) {
        await connection;
        ctx.body = await getMovies();
        await ctx.render('index',{
            movies: ctx.body}
        );
}
//Search Movie Page
async function search(ctx) {
    await connection;
    ctx.body = await getAMovie(ctx.request.body);
    await ctx.render('search',{
        movies: ctx.body}
    );        
}

//Update a Movie Plot
async function update(ctx) {
    await connection;
    var arreglo = ctx.request.body.plot.split('/');
    var plot = arreglo[1];
    var titulo = arreglo[0];
    return new Promise((resolve, reject) => {
    request(url+apikey+'&t='+titulo, { json: true }, (err, res, body) => {
        const listMovie = Movies.find({"title":String(body.Title)}).update();
        listMovie.exec((er, res) => {
            if (er) {  
                console.log(er);
                ctx.redirect('/');   
            }
            else { 
                resolve(res);
            }
        });  
       });    
    });
}

//Add Movie Page
async function showAdd(ctx) {
    await ctx.render('add');    
}

//Add Movie Method
async function add(ctx) {
    //Se Obtiene el nombre de la película del formulario
    var bodyAdd = ctx.request.body;
    //se levanta conexión
    await connection;
    //Buscamos en la API y traemos los datos en formato JSON para insertarlos
    request(url+apikey+'&t='+bodyAdd.nombre, { json: true }, (err, res, body) => {
        if (err) { return console.log(err); }
        //Compara si la película ingresada se encuentra ya previamente ingresada al comparar con array de ID IMDB        
        var found = listaID.find(element => element = body.imdbID);
        //Si la película no se encuentra repetida inserta, sino indica que es una película repetida
        if (!found){
            var movies = new Movies({
                title: body.Title,
                year: body.Year,
                released: body.Released,
                genre: body.Genre,
                director: body.Director,
                actors: body.Actors,
                plot: body.Plot,
                ratings: body.Ratings
            });
            movies.save(function(err,res){
                if (err) return console.error(err);
                listaID.push(body.imdbID);
                console.log('Película ingresada con éxito');
            })
            
        } else {
            console.log('Película repetida');
        }
        
    });
    ctx.redirect('/');
}

//Function que trae la lista de películas para mostrar en el INDEX
function getMovies() {
    return new Promise((resolve, reject) => {
       const listMovie = Movies.find({});
       listMovie.exec((er, res) => {
         if (er) {  reject(er);   }
         else { 
             resolve(res);
         }
       });        
    });
  }

//Function que trae una película para mostrar en SEARCH
async function getAMovie(params) {
    await connection;
    return new Promise((resolve, reject) => {
       request(url+apikey+'&t='+params.title, { json: true }, (err, res, body) => {
        const listMovie = Movies.find({"title":String(body.Title)});
        listMovie.exec((er, res) => {
            if (er) {  reject(er);   }
            else { 
                resolve(res);
            }
        });  
       });      
    });
  }



//Configuración de Middleware de rutas
app.use(router.routes()).
    use(router.allowedMethods());

//Se deja escuchando el servidor en puerto 3000
app.listen(port, () => 
console.log('Servidor Koa iniciado con éxito'));